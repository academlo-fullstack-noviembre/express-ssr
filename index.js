const express = require('express');
const exphbs = require('express-handlebars');
const {clients} = require('./models')

const app = express();

app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');

let users = [
    "Paul",
    "Sergio",
    "Yolanda",
    "Andrea",
    "Sandra"
]

//SSR -> server side rendering (Node, laravel, django, etc...)
//CSR -> client side rendering (React, Angular, Vue)
app.get("/", async (req, res) => {
    const results = await clients.findAll({raw: true});
    console.log(results);
    res.render('home', { 
        title: "Bienvenido al sitio web Academlo", 
        message: "hello world",
        users: results
    });
});

app.get("/contacto", (req, res) => {
    res.render('contact', { 
        title: "Contacto", 
        message: "hello world" 
    });
});

app.get("/acerca", (req, res) => {
    res.render('about', { 
        title: "Acerca de nosotros", 
        message: "hello world" 
    });
});

app.listen(8000, () => {
    console.log("Servidor escuchando sobre el puerto 8000");
});